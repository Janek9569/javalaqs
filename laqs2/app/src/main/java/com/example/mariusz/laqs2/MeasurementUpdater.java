package com.example.mariusz.laqs2;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MeasurementUpdater extends Activity
{
    private MapsActivity _mapsActivity;
    private Map<String, Mark> _marks;//street name, mark
    private Timer timer;
    private final String rawFileName = "RawMeasurements.csv";
    private int measurementIter;

    public Mark getMark(String name)
    {
        return _marks.get(name);
    }

    public class Mark
    {
        private double _longitude;
        private double _latitude;
        private ArrayList<Measurement> _measurements;//dateandtime, measurement

        private Mark(double longitude, double latitude)
        {
            _longitude = longitude;
            _latitude = latitude;
            _measurements = new ArrayList<>();
        }

        public double getLongnitude() {
            return _longitude;
        }

        public double getLatitude() {
            return _latitude;
        }

        public ArrayList<Measurement> getMeasurements() {
            return _measurements;
        }

        public void addMeasurement(String dateAndTime, Measurement newMeasurement)
        {
            _measurements.add(newMeasurement);
        }
    }

    public class Measurement
    {

        private String _dateAndTime;
        private float _windVelocity;
        private float _windDirection;
        private float _humidity;
        private float _windTemperature;
        private float _groundTemperature;
        private int _rainFall;

        private Measurement(String dateAndTime, float windVel, float windDir, float humidity,
                           float windTemperature, float groundTemperature, int rainFall)
        {
            _dateAndTime = dateAndTime;
            _windVelocity = windVel;
            _windDirection = windDir;
            _humidity = humidity;
            _windTemperature = windTemperature;
            _groundTemperature = groundTemperature;
            _rainFall = rainFall;
        }

        public String getDateAndTime() {
            return _dateAndTime;
        }

        public float getWindVelocity() {
            return _windVelocity;
        }

        public float getWindDirection() {
            return _windDirection;
        }

        public float getHumidity() {
            return _humidity;
        }

        public float getWindTemperature() {
            return _windTemperature;
        }

        public float getGroundTemperature() {
            return _groundTemperature;
        }

        public int getRainFall() {
            return _rainFall;
        }
    }

    public MeasurementUpdater(MapsActivity mapsActivity)
    {
        measurementIter = 0;
        _mapsActivity = mapsActivity;
        _marks = new HashMap<>();

        Mark newMark = new Mark(16.935564, 51.138133);
        _marks.put("UL. LOTNICZA / UL. KOSMONAUT�W", newMark);

        newMark = new Mark(17.017885, 51.117464);
        _marks.put("MOST ROMANA DMOWSKIEGO", newMark);

        newMark = new Mark(17.154913, 51.162233);
        _marks.put("AL. JANA III SOBIESKIEGO", newMark);

        newMark = new Mark(17.084217, 51.077172);
        _marks.put("UL. OPOLSKA / UL. KATOWICKA", newMark);

        newMark = new Mark(16.96892, 51.115841);
        _marks.put("ESTAKADA GADOWIANKA", newMark);

        newMark = new Mark(16.992248, 51.133352);
        _marks.put("MOST MILENIJNY", newMark);

        newMark = new Mark(17.057962, 51.129456);
        _marks.put("MOST WARSZAWSKI", newMark);

        placeAllKnownMarks();
        //To do: remove hardcoded measurements

        timer = new Timer();
        timer.schedule(new UpdateTask(), 1);
    }

    private void placeAllKnownMarks()
    {
        for (String markName : _marks.keySet())
        {
            GoogleMap map = _mapsActivity.getMap();
            Mark knownMark = _marks.get(markName);
            LatLng newMark = new LatLng(knownMark.getLatitude(), knownMark.getLongnitude());
            map.addMarker(new MarkerOptions().position(newMark).title(markName));
        }
    }

    private class UpdateTask extends TimerTask
    {
        private final int nextUpdate = 11* 60 *1000;
        public void run()
        {
            DownloadFilesTask download = new DownloadFilesTask();
            try {
                download.execute(new URL("http://www.wroclaw.pl/open-data/opendata/its/pogoda/pogoda.csv"));
            } catch (MalformedURLException m) {}


            timer.cancel(); //Wyłączamy taska
            timer = new Timer();
            timer.schedule(new UpdateTask(), nextUpdate);
        }

    }


    private class DownloadFilesTask extends AsyncTask<URL, Integer, Long>
    {
        protected Long doInBackground(URL... urls)
        {
            long ret = 0;
            copyUrlToFile(urls[0], rawFileName);

            if (isCancelled())
                ret = 1;

            return ret;
        }

        private int testUpdater = 999;
        private void updateMeasurements()
        {
            /*
            //to do: fill this function
            ++testUpdater;
            for(Mark x : _marks.values())
            {
                x.getMeasurements().add(new Measurement(""+testUpdater, testUpdater,
                        testUpdater,testUpdater,testUpdater,testUpdater,testUpdater));
            }
            */
        }

        protected void onPostExecute(Long result)
        {
            updateMeasurements();
        }


        private void parseFile(String fName)
        {
            String filePath = _mapsActivity.getContext().getFilesDir().getPath() + fName;
            File openFile = new File(filePath);
            StringBuilder text = new StringBuilder();
            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(openFile));
                String line = br.readLine();
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(";");
                    Measurement newMeas = new Measurement(values[0],Float.parseFloat(values[1]), Float.parseFloat(values[2]), Float.parseFloat(values[3]), Float.parseFloat(values[4]), Float.parseFloat(values[5]),Integer.parseInt(values[6]));
                    _marks.get(values[7]).getMeasurements().add(newMeas);
                }
                br.close();
            }
            catch (IOException e) {
                //You'll need to add proper error handling here
            }
        }

        private void copyUrlToFile(URL url, String fName)
        {
            OutputStream outStream = null;
            URLConnection uCon = null;

            InputStream is = null;
            try {
                byte[] buf;
                int byteRead;
                String filePath = _mapsActivity.getContext().getFilesDir().getPath() + fName;
                File newFile = new File(filePath);
                outStream = new BufferedOutputStream(new FileOutputStream(newFile));

                uCon = url.openConnection();
                is = uCon.getInputStream();
                buf = new byte[4096];
                while ((byteRead = is.read(buf)) != -1) {
                    outStream.write(buf, 0, byteRead);
                }
                parseFile(fName);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
