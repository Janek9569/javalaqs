package com.example.mariusz.laqs2;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import java.util.ArrayList;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class StationInfo extends AppCompatActivity {
    int[] IMAGES = {R.drawable.sun,R.drawable.cloudsun,R.drawable.rain,R.drawable.rainsun};


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Station Measurements");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView list = (ListView) findViewById(R.id.measurement_list);
        MeasurementUpdater updater = MapsActivity.getUpdater();
        Bundle extras = getIntent().getExtras();
        String markName = extras.getString(EXTRA_MESSAGE);
        MeasurementUpdater.Mark mark = updater.getMark(markName);

        ArrayList<MeasurementUpdater.Measurement> measurements = mark.getMeasurements();
        list.setAdapter(new MeasurementAdapter(this, measurements));

        printDistance(mark);
    }

    private void printDistance(MeasurementUpdater.Mark mark)
    {
        TextView distanceTextVeiw = (TextView) findViewById(R.id.distance_text);
        GpsInfo markPosition = new GpsInfo(mark.getLongnitude(), mark.getLatitude());
        String distanceInfo = "Distance: " + calcDistanceKm(getMyPosition(), markPosition) + " km";
        distanceTextVeiw.setText(distanceInfo);
    }

    private double degToRad(double deg)
    {
        return deg * Math.PI / 180;
    }

    private double calcDistanceKm(GpsInfo gpsInfo1, GpsInfo gpsInfo2)
    {
        final int earthRadiusKm = 6371;
        double dLat = degToRad(gpsInfo2.getLatitude() - gpsInfo1.getLatitude());
        double dLon = degToRad(gpsInfo2.getLongitde() - gpsInfo1.getLongitde());
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(gpsInfo1.getLatitude()) * Math.cos(gpsInfo2.getLatitude());
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadiusKm * c;
    }

    private class GpsInfo
    {
        private double _longitude;
        private double _latitude;

        private GpsInfo(double longitude, double latitude)
        {
            _longitude = longitude;
            _latitude = latitude;
        }

        private double getLongitde()
        {
            return _longitude;
        }

        private double getLatitude()
        {
            return _latitude;
        }
    }

    private GpsInfo getMyPosition()
    {
        //To do: fill this function
        return new GpsInfo(17.05957844, 51.10938662);
    }


    private class MeasurementAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<MeasurementUpdater.Measurement> measurements;

        private MeasurementAdapter(Context context, ArrayList<MeasurementUpdater.Measurement> measurements) {
            this.context = context;
            this.measurements = measurements;
        }

        @Override
        public int getCount() {
            return measurements.size();
        }

        @Override
        public Object getItem(int position) {
            return measurements.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.custom_layout,null);

            ImageView img   = (ImageView)view.findViewById(R.id.imageView);
            TextView  text1 = (TextView) view.findViewById(R.id.textView_timestamp);
            TextView  text2 = (TextView)view.findViewById(R.id.textView_data);



            text1.setText(measurements.get(position).getDateAndTime());

            String subItemContent = "    Wind velocity: "         + measurements.get(position).getWindVelocity()          + '\n'+
                                    "    Wind temperature: "      + measurements.get(position).getWindTemperature()       + '\n'+
                                    "    Wind direction: "        + measurements.get(position).getWindDirection()         + '\n'+
                                    "    Ground temperature: "    + measurements.get(position).getGroundTemperature()     + '\n'+
                                    "    Rain fall: "             + measurements.get(position).getRainFall()              + '\n'+
                                    "    Humidity: "              + measurements.get(position).getHumidity()                    ;

            text2.setText(subItemContent);

            if(measurements.get(position).getRainFall() >= 60) {
                if(measurements.get(position).getWindTemperature() > 5)
                    img.setImageResource(IMAGES[3]);
                else
                    img.setImageResource(IMAGES[2]);
            }
            else if(measurements.get(position).getGroundTemperature() >= 22)
                img.setImageResource(IMAGES[0]);
            else
                img.setImageResource(IMAGES[1]);
            return view;
        }
    }
}
